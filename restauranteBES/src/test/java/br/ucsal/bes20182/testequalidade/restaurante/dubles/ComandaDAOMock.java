package br.ucsal.bes20182.testequalidade.restaurante.dubles;


public class ComandaDAOMock {

	public void incluir(ComandaMock comanda) {
		comanda.setCodigo(1);
	}
	
	public ComandaMock obterPorCodigo(Integer numero) {
		ComandaMock comanda = new ComandaMock(1);
		return comanda;
	}

}
