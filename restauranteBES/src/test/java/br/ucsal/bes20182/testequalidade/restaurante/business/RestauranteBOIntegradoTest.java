package br.ucsal.bes20182.testequalidade.restaurante.business;

import org.junit.Test;
import org.junit.Before;
import org.junit.Assert;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20182.testequalidade.restaurante.dubles.ComandaDAOMock;
import br.ucsal.bes20182.testequalidade.restaurante.dubles.ComandaFechadaExceptionMock;
import br.ucsal.bes20182.testequalidade.restaurante.dubles.ComandaMock;
import br.ucsal.bes20182.testequalidade.restaurante.dubles.ItemDAOMock;
import br.ucsal.bes20182.testequalidade.restaurante.enums.SituacaoComandaEnum;



public class RestauranteBOIntegradoTest {

	// M�todo a ser testado: public void incluirItemComanda(Integer
	// codigoComanda, Integer codigoItem, Integer qtdItem) throws
	// RegistroNaoEncontrado, ComandaFechadaException {
	// Verificar se a inclus�o de uma item em uma comanda fechada levanta
	// exce��o.
	private ComandaDAOMock comandaDAOMock;
	private ItemDAOMock itemDAOMock;
	
	@Before
	public void setup() {
		comandaDAOMock = new ComandaDAOMock();
		itemDAOMock = new ItemDAOMock();
	}
	
	@Test
	public void incluirItemComandaFechada() throws ComandaFechadaExceptionMock {
		ComandaMock comanda = comandaDAOMock.obterPorCodigo(2);
		Item item = itemDAOMock.obterPorCodigo(2);
		comanda.setSituacao(SituacaoComandaEnum.FECHADA);
		Assert.assertEquals(false, comanda.getExceptionChamado());
		comanda.incluirItem(item, 2);
		Assert.assertEquals(true, comanda.getExceptionChamado());
	}
}
