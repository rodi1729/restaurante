package br.ucsal.bes20182.testequalidade.restaurante.business;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.dubles.ComandaDAOMock;
import br.ucsal.bes20182.testequalidade.restaurante.dubles.ComandaMock;
import br.ucsal.bes20182.testequalidade.restaurante.dubles.MesaDAOMock;

public class RestauranteBOUnitarioTest {

	// M�todo a ser testado: public Integer abrirComanda(Integer numeroMesa)
	// throws RegistroNaoEncontrado, MesaOcupadaException {
	// Verificar se a abertura de uma comanda para uma mesa livre apresenta
	// sucesso.
	
	private MesaDAOMock mesaDAOMock;
	private Mesa mesa;
	private ComandaMock comandaMock;
	private ComandaDAOMock comandaDAOMock;
	
	@Before
	public void setup() {
		mesaDAOMock = new MesaDAOMock();
		comandaDAOMock = new ComandaDAOMock();
	}
	
	
	@Test
	public void abrirComandaMesaLivre() {
		mesa = mesaDAOMock.obterPorNumero(1);
		comandaMock = new ComandaMock(mesa);
		comandaDAOMock.incluir(comandaMock);
		Integer respostaEsperada = comandaMock.getCodigo();
		Integer respostaAtual = 1;
		Assert.assertEquals(respostaEsperada, respostaAtual);
		
	}
}
